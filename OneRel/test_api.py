from torch.nn import CrossEntropyLoss
import torch
import torch.nn.functional as F

input = torch.randn(2,3,5,5,4)
target = torch.empty(2,3,5,5, dtype=torch.long).random_(4)
loss_fn=CrossEntropyLoss(reduction='mean')
_input=torch.permute(input,dims=(0,-1,1,2,3))
loss=loss_fn(_input,target)
print(loss)
target=F.one_hot(target,num_classes=4).type(torch.float)
loss=loss_fn(input,target)
print(loss)
# loss = CrossEntropyLoss()
# input = torch.randn(3, 5, 4)
# target = torch.empty(3,4, dtype=torch.long).random_(5)
# output = loss(input, target)
# print(output)

# target = F.one_hot(target, num_classes=5).type(torch.float64)
# output = loss(input, target)
# print(output)
