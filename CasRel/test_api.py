# -*- coding: utf-8 -*-
# @Time    : 2023/3/16 15:05
# @Author  : xiehou
# @File    : test_api.py
# @Software: PyCharm
import numpy as np

a = np.array([1, 3])
b = np.array([4, 5])
print(b >= a)
print(b[b >= a])
