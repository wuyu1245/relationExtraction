# -*- coding: utf-8 -*-
# @Time    : 2023/3/18 14:17
# @Author  : xiehou
# @File    : test_api.py
# @Software: PyCharm
import torch
from transformers import AutoModel, BertTokenizerFast, BertModel
import math

# pred = torch.tensor([[0, 0, 1, 0, 1, 2, 0, 2], [2, 2, 1, 0, 1, 2, 0, 2]])
# truth = torch.tensor([[0, 0, 1, 0, 1, 2, 0, 2], [1, 2, 1, 0, 1, 1, 0, 2]])
#
# correct_tag_num = torch.sum(torch.eq(truth, pred).float(), dim=1)
# print(correct_tag_num)
#
# # seq维上所有tag必须正确，所以correct_tag_num必须等于seq的长度才算一个correct的sample
# sample_acc_ = torch.eq(correct_tag_num, torch.ones_like(correct_tag_num) * truth.size()[-1]).float()
# print(sample_acc_)
# sample_acc = torch.mean(sample_acc_)
# print(sample_acc)
shaking_hiddens = torch.rand([2, 10, 12])

shaking_seq_len = shaking_hiddens.size()[1]
segment_len = int(shaking_seq_len * 0.2)
# 向上取整
seg_num = math.ceil(shaking_seq_len // segment_len)
start_ind = torch.randint(seg_num, []) * segment_len
print(start_ind)
end_ind = min(start_ind + segment_len, shaking_seq_len)
# sampled_tok_pair_indices: (batch_size, ~segment_len) ~end_ind - start_ind <= segment_len
sampled_tok_pair_indices = torch.arange(start_ind, end_ind)[None, :].repeat(shaking_hiddens.size()[0], 1)
sampled_tok_pair_indices = sampled_tok_pair_indices.to(shaking_hiddens.device)

shaking_hiddens = shaking_hiddens.gather(1, sampled_tok_pair_indices[:, :, None].repeat(1, 1,
                                                                                        shaking_hiddens.size()[
                                                                                            -1]))
print(shaking_hiddens.shape)
